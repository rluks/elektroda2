﻿using UnityEngine;
using System.Collections;

public class MenuLife : MonoBehaviour {

    GameObject hero;
    MeshRenderer mr;
    MenuHero mh;

    void Awake()
    {
        mh = GameObject.Find("Hero").GetComponent<MenuHero>();
        mr = GetComponent<MeshRenderer>();
    }

    // Use this for initialization
    void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            mr.enabled = false;
            mh.MenuHeal();
            Invoke("RenderMe", 4.0f);
        }
    }

    void RenderMe()
    {
        mr.enabled = true;
    }
}
