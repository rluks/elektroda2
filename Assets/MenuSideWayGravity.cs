﻿using UnityEngine;
using System.Collections;

public class MenuSideWayGravity : MonoBehaviour {

    Rigidbody rb;
    Vector3 sidewayGravity;
    Vector3 startPosition;
    TrailRenderer tr;

    // Use this for initialization
    void Start () {
        rb = GetComponent<Rigidbody>();
        tr = GetComponentInChildren<TrailRenderer>();

        sidewayGravity = new Vector3(10, 0, 0);

        rb.AddForce(400*sidewayGravity);
        rb.AddTorque(new Vector3(0, 0, -8000));

        startPosition = new Vector3(-500, -100, 185);
        transform.position = startPosition;

        InvokeRepeating("checkPosition", 1.0f, 0.5f);
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    void checkPosition()
    {
        if (transform.position.x > 1000)
        {
            transform.position = startPosition;
            tr.time = 0;
            Invoke("putTrailOn", 0.1f);
        }
    }

    void putTrailOn()
    {
        tr.time = 1;
    }
}
