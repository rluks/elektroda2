﻿using UnityEngine;
using System.Collections;

public class MenuHero : MonoBehaviour {

    Light light;
    Color bad;
    Color healed;

    void Awake()
    {
        light = GameObject.Find("Point light").GetComponent<Light>();
    }

    // Use this for initialization
    void Start () {
        bad = new Color(0.5f, 0.0f, 0.0f, 0.5f);
        healed = new Color(255/255.0f, 111 / 255.0f, 0, 255 / 255.0f);

        light.color = bad;
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void backToNormalInvoke(Color color, float time)
    {
        light.color = color;
        Invoke("backToNormal", time);
    }

    void backToNormal()
    {
        light.color = bad;
    }

    public void MenuHeal()
    {
        backToNormalInvoke(healed, 3.0f);
    }
}
