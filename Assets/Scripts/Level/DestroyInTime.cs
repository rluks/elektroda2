﻿using UnityEngine;
using System.Collections;

public class DestroyInTime : MonoBehaviour {

	float timeToDestroy = 5.0f;

	// Use this for initialization
	void Start () {
		Invoke ("DestroyMe", timeToDestroy);
	}

	void DestroyMe(){
		Destroy(this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
