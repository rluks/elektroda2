﻿using UnityEngine;
using System.Collections;

public class ObstacleManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static void disableObstacles(){
		GameObject[] obstacles = GameObject.FindGameObjectsWithTag("Obstacle");
		foreach (GameObject o in obstacles) {
			Destroy(o);
		}
	}
}
