﻿using UnityEngine;
using System.Collections;

public class SpawnLevel : MonoBehaviour {

	GameObject Coridor;

	GameObject coridorSegmentPrefab;
	GameObject coridorPrefab;
	GameObject lifeBonusPrefab;
	GameObject speedBonusPrefab;
	GameObject scaleBonusPrefab;
	GameObject coinBonusPrefab;
	
	GameObject[] topBlockers;
	GameObject[] bottomBlockers;
	
	bool collided;

	//TODO static start?

	// Use this for initialization
	void Start () {

		this.Coridor = GameObject.Find ("Coridor");
		this.collided = false;	

		coridorSegmentPrefab = Resources.Load ("coridorSegment") as GameObject;

		lifeBonusPrefab = Resources.Load ("Bonuses/lifeBonus") as GameObject;
		speedBonusPrefab = Resources.Load ("Bonuses/speedBonus") as GameObject;
		scaleBonusPrefab = Resources.Load ("Bonuses/scaleBonus") as GameObject;
		coinBonusPrefab = Resources.Load ("Bonuses/coinBonus") as GameObject;

		this.topBlockers = new GameObject[2];
		topBlockers[0] = Resources.Load ("TopObstacles/rekcolb") as GameObject;
		topBlockers[1] = Resources.Load ("TopObstacles/rekcolb-small") as GameObject;

		this.bottomBlockers = new GameObject[2];
		bottomBlockers[0] = Resources.Load ("BottomObstacles/blocker") as GameObject;
		bottomBlockers[1] = Resources.Load ("BottomObstacles/blocker-small") as GameObject;
	}

	void OnTriggerEnter(Collider other) {

		if(!this.collided){
			this.collided = true;
			float corridorStartXPos = transform.position.x+GetComponent<Renderer>().bounds.size.x;
			Vector3 pozice = new Vector3(corridorStartXPos, transform.position.y, transform.position.z);
			GameObject segment = Instantiate(coridorSegmentPrefab, pozice, transform.rotation) as GameObject;
			segment.name = "coridorSegment";
			segment.transform.parent = Coridor.transform;

			int corridorLength = (int) GetComponent<Renderer>().bounds.size.x;
			int blockerLength = (int) topBlockers[0].GetComponent<Renderer>().bounds.size.x;
			int numOfBlockers = corridorLength/blockerLength;

			for(int i = 0; i < numOfBlockers; i++){

				if(Random.Range(0, 5) > 3){
					float zPos = 1.0f;
					Vector3 blockerPozice = new Vector3(corridorStartXPos+i*2, transform.position.y, zPos);
					//GameObject blocker;
					GameObject blocker = Instantiate(topBlockers[Random.Range(0, topBlockers.Length)], blockerPozice, transform.rotation) as GameObject;
					blocker.name = "blocker";
					blocker.transform.Rotate(0, 0, 90);
					blocker.transform.parent = segment.transform;

				}else if(Random.Range(0, 7) > 5){
					float zPos = -1.0f;
					Vector3 blockerPozice = new Vector3(corridorStartXPos+i*2, transform.position.y+GetComponent<Renderer>().bounds.size.y/2-1, zPos);
					//GameObject blocker;
					GameObject blocker = Instantiate(bottomBlockers[Random.Range(0, bottomBlockers.Length)], blockerPozice, transform.rotation) as GameObject;
					blocker.name = "rekcolb";
					blocker.transform.Rotate(0, 180, 90);
					blocker.transform.parent = segment.transform;

				} else if(Random.Range (0,11) > 9) {
					SpawnLifeBonus(corridorStartXPos, i, segment);
				} else if(Random.Range (0,11) > 9) {
					SpawnBonus(corridorStartXPos, i, segment);
				}
			}
		}
	}

	void SpawnLifeBonus(float corridorStartXPos, int i, GameObject segment){
		float zPos = 0.0f;
		float yPos = Random.Range(transform.position.y+1, transform.position.y+4.5f);
		Vector3 lifePozice = new Vector3(corridorStartXPos+i*2, yPos, zPos);
		GameObject life = Instantiate(lifeBonusPrefab, lifePozice, transform.rotation) as GameObject;
		life.name = "lifeBonus";
		life.transform.parent = segment.transform;
	}
	
	
	GameObject GetRandomBonusPrefab(){

		GameObject bonusPrefab;
		float random = Random.value;

		if(random <= 1/3.0f)
			bonusPrefab = speedBonusPrefab;
		else if(random > 1/3.0f && random <= 2/3.0f)
			bonusPrefab = scaleBonusPrefab;
		else
			bonusPrefab = coinBonusPrefab;

        //Testing line, assing what you want to spawn
		//bonusPrefab = scaleBonusPrefab;

		return bonusPrefab;
	}

	void SpawnBonus(float corridorStartXPos, int i, GameObject segment){
		GameObject bonusPrefab = GetRandomBonusPrefab();
		float zPos = 0.0f;
		float yPos = Random.Range(transform.position.y+1, transform.position.y+5);
		Vector3 position = new Vector3(corridorStartXPos+i*2, yPos, zPos);
		//GameObject bonus;
		GameObject bonus = Instantiate(bonusPrefab, position, Quaternion.Euler(0, 0, 0)) as GameObject;
		//Debug.Log ("a rotation"+bonus.transform.rotation);
		//Debug.Log ("a localrotation"+bonus.transform.localRotation );
		//Debug.Log ("a localeuler "+bonus.transform.localEulerAngles );
		bonus.name = bonusPrefab.name;
		bonus.transform.parent = segment.transform;
		//Debug.Log ("b rotation"+bonus.transform.rotation);
		//Debug.Log ("b localrotation"+bonus.transform.localRotation);
		//Debug.Log ("b localeuler"+bonus.transform.localEulerAngles);
		
		if(bonusPrefab == speedBonusPrefab){
			bonus.transform.localEulerAngles = new Vector3(0, 0, 0);
		}
		//Debug.Log (bonus.name+" pos"+bonus.transform.position.y);
		//Debug.Log ("c rotation"+bonus.transform.rotation);
		//Debug.Log ("c localrotation"+bonus.transform.localRotation);
		//Debug.Log ("c localeuler"+bonus.transform.localEulerAngles);
	}
	
	
}
