using UnityEngine;
using System.Collections;

public class Ovladani : MonoBehaviour {

	public KeyCode MoveUp;
	public KeyCode MoveDown;

	public float MaxSpeed = 20.0f;
	int MaxLife = 1000;
	public int PlayerLife;
	
	public float lasttime = 0;
	public int startForce = 10;
	public int jumpForce = 10;
	public int collisionExitForce = 50;
	public int verticalForce = 25;
	public float slowVerticalMultiplier = 1.5f;
	public int lifeDecrement = 5;

	public AudioSource SoundJizdy;
	public float soundInterval = 0.01f;

	int lastDistance;

	GameObject scalableHero;
	InfoText infoText;
	Light light;

	void Awake(){
		infoText = GameObject.Find ("infoText").GetComponent<InfoText>();
		scalableHero = GameObject.Find ("HeroScalable");
		light = GameObject.Find ("Point light").GetComponent<Light> ();
	}

	// Use this for initialization
	void Start () {
		GetComponent<Rigidbody>().AddForce(startForce, 0, 0);
		InvokeRepeating("lowerLife", 0.1f, 0.1f);
		lastDistance = 0;
		PlayerLife = MaxLife;
	}

	void  OnCollisionExit (Collision collisionInfo){
		GetComponent<Rigidbody>().AddForce(collisionExitForce, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {

		float x = transform.position.x;
		float i = soundInterval * GetComponent<Rigidbody>().velocity.x;
		if (Mathf.CeilToInt(x-i)%5 == 0 && Mathf.FloorToInt(x+i)%5 == 0) {
			if (SoundJizdy != null) {
				SoundJizdy.Play();
			}
		}

		if (PlayerLife <= 0 && GameManager.running) {
			GameManager.instance.End();
		}
		light.intensity = PlayerLife/100.0f;
	}

	void  FixedUpdate (){
		GameManager.playerSpeed = (int) Mathf.Abs(GetComponent<Rigidbody>().velocity.x);
			
		if (((int)transform.position.x) % 3 == 0 && GetComponent<Rigidbody>().velocity.x > 0) {
			if(lastDistance != ((int)transform.position.x)){
				ScoreManager.AddDistance();
				lastDistance = (int)transform.position.x;
			}
		}
		
		if ((Input.GetKey(MoveUp) || (Input.touchCount > 0 && Input.GetTouch(0).position.x > Screen.width/2)) 
		    && GameManager.running) {
			GetComponent<Rigidbody>().AddForce(0,verticalForce,0);
			if (GetComponent<Rigidbody>().velocity.x < MaxSpeed) {
				GetComponent<Rigidbody>().AddForce(jumpForce,0,0);
			}
		} else if ((Input.GetKey(MoveDown) || (Input.touchCount > 0 && Input.GetTouch(0).position.x < Screen.width/2))
		           && GameManager.running) {
			GetComponent<Rigidbody>().AddForce(0,-verticalForce,0);
			if (GetComponent<Rigidbody>().velocity.x >= 0) {
				GetComponent<Rigidbody>().AddForce(jumpForce,0,0);
			}
		}
	}

	void  lowerLife (){
		PlayerLife -= lifeDecrement;

		if (PlayerLife == MaxLife / 2)
			infoText.Prompt ("Light is fading! Pick hearts!");
		if (PlayerLife == MaxLife / 5)
			infoText.Prompt ("Danger!\nLight almost gone! Pick hearts!");
	}

	public void Heal(int points){
		PlayerLife += points;
		if(PlayerLife > MaxLife)
			PlayerLife = MaxLife;

		backToNormalInvoke(new Color(0.5f, 0.0f, 0.0f, 0.5f), 0.3f);
		infoText.ClearString ();    
    }

	public void  backToNormalInvoke ( Color color ,   float time  ){
		light.color = color;
		Invoke("backToNormal",time);
	}

	void  backToNormal (){
		light.color = new Color(1, 111/255.0f, 0, 1);
	}

	public void ScaleBonus(){
		scalableHero.GetComponent<ScaleEffect> ().scaleBonus ();
		backToNormalInvoke(new Color(0.5f, 0.5f, 0.0f, 0.5f), 0.4f);
	}

	public void AddForce(float force){
		GetComponent<Rigidbody>().AddForce(force, 0.0f, 0.0f);
		backToNormalInvoke(new Color(0.0f, 0.5f, 0.0f, 0.5f), 0.4f);
	}

	public void CoinBonus(){
        ScoreManager.AddBonus();
        ScoreManager.AddBonus();
        ScoreManager.AddBonus();
    }
}
