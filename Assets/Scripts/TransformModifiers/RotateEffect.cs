﻿using UnityEngine;
using System.Collections;

public class RotateEffect : MonoBehaviour {

	public float rotate_speed = 250.0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(0, rotate_speed*Time.deltaTime, 0, Space.World);
	}
}
