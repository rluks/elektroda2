﻿using UnityEngine;
using System.Collections;

public class ScaleEffect : MonoBehaviour {

	public void scaleBonus(){
		InvokeRepeating ("scaleDown", 0.1f, 0.2f);
	}
	
	void scaleDown(){
		transform.localScale -= new Vector3(0.05f, 0.05f, 0.05f);
		if (transform.localScale.x < 0.7f) {
			CancelInvoke("scaleDown");
			InvokeRepeating ("scaleBack", 0.1f, 0.5f);
		}
	}
	
	void scaleBack(){
		transform.localScale += new Vector3(0.01f, 0.01f, 0.01f);
		if (transform.localScale.x > 1.0f) {
			CancelInvoke("scaleBack");
			transform.localScale = new Vector3 (1.0f, 1.0f, 1.0f);
		}
	}
}
