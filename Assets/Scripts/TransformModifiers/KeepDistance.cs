﻿using UnityEngine;
using System.Collections;

public class KeepDistance : MonoBehaviour {

	public Transform target;
	public float distance;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void  FixedUpdate (){
		transform.position = new Vector3(target.position.x + distance, target.position.y, target.position.z);
	}
}
