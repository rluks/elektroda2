﻿using UnityEngine;
using System.Collections;

public class ScoreManager : MonoBehaviour {

	public static int playerScore = 0;

	void  Awake (){
		if (!PlayerPrefs.HasKey("highscore")) {
			PlayerPrefs.SetInt("highscore", 0);
		}
	}

	// Use this for initialization
	void Start () {
		playerScore = 0;

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static int GetHighScore(){
		int highest = PlayerPrefs.GetInt("highscore");

		return highest;
	}

	public static bool isHighScore(){
		if (playerScore >= GetHighScore()) {
			PlayerPrefs.SetInt ("highscore", playerScore);
			return true;
		}else{
			return false;
		}
	}

	public static void  AddDistance (){
		playerScore += 1;
	}

	public static void AddBonus(){
		playerScore += 20;
	}
}
