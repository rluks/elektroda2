﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager instance { get; private set; }

	public static int playerSpeed = 0;
	public static bool  running = true;
	public static bool  ended = false;
	
	PauseMenu pauseMenu;
	EndMenu endMenu;

	void Awake(){
		if (instance != null)
			Debug.LogError ("Singleton error");
		instance = this;

		//Debug.Log ("asssets:..asi uz nevali v Unity5");
		//var assetList = UsedAssets.GetList ();
		//foreach (var assetItem in assetList) {
		//	Debug.Log (assetItem);
		//}

		pauseMenu = GameObject.Find ("pauseMenu").GetComponent<PauseMenu>();
		endMenu = GameObject.Find ("endMenu").GetComponent<EndMenu>();
	}

	// Use this for initialization
	void Start () {
		running = true;
		ended = false;
		playerSpeed = 0;
		Time.timeScale = 1;
	}

	public void Pause(){
		pauseMenu.Enable();
		Time.timeScale = 0;
		running = false;
	}

	public void Continue(){
		pauseMenu.Disable();
		running = true;
		Time.timeScale = 1;
	}
	
	public void Restart(){
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
	}

	public void End(){
		ended = true;
		endMenu.Enable ();
		Time.timeScale = 0;
		running = false;
	}

	public void ReturnToMenu(){
        SceneManager.LoadScene("scene_main");
	}

	// Update is called once per frame
	void Update () {
		if (running) {
			if (Input.GetKeyDown (KeyCode.Escape)) {
				Pause ();
			}
		}

		if (ended) {
			End ();
		}
	}
}
