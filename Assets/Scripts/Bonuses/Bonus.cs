﻿using UnityEngine;
using System.Collections;

public class Bonus : MonoBehaviour {
	
	protected Ovladani ovladani;

	void Awake(){
		GameObject Hero = GameObject.Find("Hero");
		ovladani = Hero.GetComponent<Ovladani> ();
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	protected void myOnTrigger(Collider other){
		ScoreManager.AddBonus ();
		Destroy(gameObject);
	}
}
