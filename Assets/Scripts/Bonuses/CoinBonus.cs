﻿using UnityEngine;
using System.Collections;

public class CoinBonus : Bonus {

    AudioSource bonusSound;

    // Use this for initialization
    void Start () {
        bonusSound = GameObject.Find("SoundCoin").GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update () {
	
	}

	void  OnTriggerEnter (Collider other){
		if (other.tag == "Player") {
            if (bonusSound != null)
            {
                bonusSound.Play();
            }
            base.ovladani.CoinBonus();
			base.myOnTrigger(other);
		}
	}
}
