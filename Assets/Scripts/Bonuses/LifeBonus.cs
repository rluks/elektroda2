﻿using UnityEngine;
using System.Collections;

public class LifeBonus : Bonus {

	public int lifeIncrement = 300;

    AudioSource bonusSound;

    void Start () {
        bonusSound = GameObject.Find("SoundLife").GetComponent<AudioSource>();
    }

	void  OnTriggerEnter (Collider other){
		if (other.tag == "Player") {
            if (bonusSound != null)
                bonusSound.Play();
            base.ovladani.Heal(lifeIncrement);
			base.myOnTrigger(other);
		}
	}
}
