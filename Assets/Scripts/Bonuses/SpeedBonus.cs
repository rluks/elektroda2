﻿using UnityEngine;
using System.Collections;

public class SpeedBonus : Bonus {

	float force = 300.0f;

	AudioSource ZvukKolize;

	// Use this for initialization
	void Start () {
		ZvukKolize = GameObject.Find("SoundBolt").GetComponent<AudioSource>();
	}

	void  OnTriggerEnter ( Collider other  ){
		if (other.tag == "Player") {
			if (ZvukKolize != null)
				ZvukKolize.Play();
			ObstacleManager.disableObstacles();
			base.ovladani.AddForce(force);
			base.myOnTrigger(other);
		}
	}
}
