﻿using UnityEngine;
using System.Collections;

public class ScaleBonus : Bonus {

    AudioSource bonusSound;

    // Use this for initialization
    void Start () {
        bonusSound = GameObject.Find("SoundScale").GetComponent<AudioSource>();
    }

    void  OnTriggerEnter ( Collider other  ){
		if (other.tag == "Player") {
            if (bonusSound != null)
                bonusSound.Play();
            base.ovladani.ScaleBonus();
			base.myOnTrigger(other);
		}
	}
}
