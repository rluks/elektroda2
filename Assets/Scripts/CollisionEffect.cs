﻿using UnityEngine;
using System.Collections;

public class CollisionEffect : MonoBehaviour {

	//GameObject coridor;
	GameObject collisionEffectPrefab;

	AudioSource zvukKolize;
	GameObject hero;

	// Use this for initialization
	void Start () {
		//this.coridor = GameObject.Find ("Coridor");
		this.hero = GameObject.Find ("Hero");
		this.collisionEffectPrefab = Resources.Load ("cubesParticleEffect") as GameObject;
		this.zvukKolize = GameObject.Find ("SoundKolize").GetComponent<AudioSource>();
	}

	void OnCollisionEnter(Collision collision) {

		if(collision.relativeVelocity.magnitude > 20){
			
			if(zvukKolize != null)
				zvukKolize.Play ();
			
			foreach (ContactPoint contact in collision.contacts) {
				GameObject efekt = Instantiate(collisionEffectPrefab, contact.point, Quaternion.LookRotation(-contact.normal)) as GameObject;

				if(efekt != null)
					efekt.transform.parent = hero.transform;
			}
		}
	}
}
