using UnityEngine;
using System.Collections;

public class InGameMenu : MonoBehaviour {

	protected InfoText infoText;
	
	void Awake(){
		infoText = GameObject.Find ("infoText").GetComponent<InfoText>();
	}
	
	// Use this for initialization
	void Start () {
		Disable ();
	}
	
	public void Disable(){
		GetComponent<Canvas> ().enabled = false;
		infoText.SetText("");
	}
}
