﻿using UnityEngine;
using System.Collections;

public class EndMenu : InGameMenu {
	
	public void Enable(){
		int highest = ScoreManager.GetHighScore();
		string histr = ""+highest;

		if (ScoreManager.isHighScore()) {
			histr = "NEW HIGHSCORE!";
		}
		base.infoText.SetText("Game over!!\nYour score is: "+ScoreManager.playerScore+"\nBest: "+histr);

		GetComponent<Canvas> ().enabled = true;
	}
}
