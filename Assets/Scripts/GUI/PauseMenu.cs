﻿using UnityEngine;
using System.Collections;

public class PauseMenu : InGameMenu {

	public void Enable(){
		int highest = ScoreManager.GetHighScore();
		string histr = ""+highest;

		infoText.SetText("PAUSE\nYour score is: "+ScoreManager.playerScore+"\nBest: "+histr);

		GetComponent<Canvas> ().enabled = true;
	}
}
