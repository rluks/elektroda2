﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Escape)){
			Application.Quit();
		} else if (Input.GetKeyDown(KeyCode.LeftControl) ||
		           Input.GetKeyDown(KeyCode.RightControl)) {
			SceneManager.LoadScene("loading");
		} else {
			for (int i= 0; i < Input.touchCount; ++i) {
				if (Input.GetTouch(i).phase == TouchPhase.Began) {
                    SceneManager.LoadScene("loading");
				}
			}
		}
	}
}
