﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InfoText : MonoBehaviour {

	public void SetText(string s){
		GetComponent<Text> ().text = s;
	}

	public void Prompt(string s){
		SetText (s);
		Invoke ("ClearString", 2.0f);
	}

	public void ClearString(){
		SetText ("");
	}

}
